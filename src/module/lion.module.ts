import { Module } from '@nestjs/common';
import { LionController } from 'src/controllers/lion.controller';
import { LionService } from 'src/services/lion.service';
import { TigerModule } from './tiger.module';

@Module({
  imports: [TigerModule],
  controllers: [LionController],
  providers: [LionService],
})
export class LionModule {}
