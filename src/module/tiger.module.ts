import { Module } from '@nestjs/common';
import { TigerController } from 'src/controllers/tiger.controller';
import { TigerService } from 'src/services/tiger.service';

@Module({
  imports: [],
  controllers: [TigerController],
  providers: [TigerService],
  exports: [TigerService],
})
export class TigerModule {}
