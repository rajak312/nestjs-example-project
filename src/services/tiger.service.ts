import { Injectable } from '@nestjs/common';
import { Tiger } from 'src/dto/tiger.dto';

@Injectable()
export class TigerService {
  private tigers: Tiger[] = [{ id: '12', name: 'Tiger', age: 10 }];
  public getTigers(): Tiger[] {
    return this.tigers;
  }
}
