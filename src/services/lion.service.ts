import { Injectable } from '@nestjs/common';
import { LionDto } from 'src/dto/lion.dto';

@Injectable()
export class LionService {
  private lions: LionDto[] = [];

  getLions(): LionDto[] {
    return this.lions;
  }
}
