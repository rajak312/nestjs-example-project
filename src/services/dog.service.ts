import { Injectable } from '@nestjs/common';
import { Dog } from 'src/dto/dog.dto';

@Injectable()
export class DogService {
  private dogs: Dog[] = [];

  create(dog: Dog) {
    this.dogs.push(dog);
    return { success: true, message: 'Dog created successfully', dog };
  }

  getAllDogs(): Dog[] {
    return this.dogs;
  }
}
