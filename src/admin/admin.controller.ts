import { Controller, Get } from '@nestjs/common';

@Controller({ host: 'google.com' })
export class AdminController {
  @Get()
  index(): string {
    return 'Admin';
  }
  @Get('me/admin')
  getMe(): string {
    return 'Me';
  }
}
