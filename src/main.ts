import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config } from 'dotenv';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

config();

async function bootstrap() {
  const port = Number(process.env.PORT);

  const app = await NestFactory.create(AppModule, {
    snapshot: true,
  });
  const options = new DocumentBuilder()
    .setTitle("NestJS API's")
    .setDescription('The NestJS API description')
    .setVersion('0.0.1')
    .addTag("API's")
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(port);
}
bootstrap();
