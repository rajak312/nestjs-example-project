import { Module } from '@nestjs/common';
import { AdminController } from './admin/admin.controller';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './catsController/cats.controller';
import { DogController } from './controllers/Dog.controller';
import { DogService } from './services/dog.service';
import { LionModule } from './module/lion.module';
import { TigerModule } from './module/tiger.module';

@Module({
  imports: [LionModule, TigerModule],
  controllers: [AppController, AdminController, CatsController, DogController],
  providers: [AppService, DogService],
})
export class AppModule {}
