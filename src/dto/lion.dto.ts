export interface LionDto {
  name: string;
  age: number;
  address?: string;
}
