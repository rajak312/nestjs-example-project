import { Optional } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Users } from 'src/enum/user.enum';

export class Dog {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty({
    type: Number,
    description: 'Age of the dog',
    minimum: 1,
    maximum: 30,
    default: 10,
  })
  age: number;
  @ApiProperty()
  @Optional()
  description: string;
  @ApiProperty({
    type: [String],
  })
  owners: string[];
  @ApiProperty({ enum: ['Admin', 'Moderator', 'User'] })
  user: Users;
}
