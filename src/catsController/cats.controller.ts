import { Get, Controller, Body, Post, Res, HttpStatus } from '@nestjs/common';
import { CreateCatDto } from './cats-dto';
import { Response } from 'express';

@Controller('cat')
export class CatsController {
  @Get('cats')
  async findAll(): Promise<any[]> {
    return ['This action returns all cats', 'Hello', 'world'];
  }
  @Post('mat')
  async create(@Body() createDto: CreateCatDto[]) {
    return `This action adds the new CAt to the database ${JSON.stringify(
      createDto,
    )}`;
  }

  @Post('get-create')
  getCreate(@Res() res: Response) {
    return res.status(HttpStatus.OK).json([]);
  }

  @Get('all-cats')
  findAllCats(@Res({ passthrough: true }) res: Response) {
    res.status(HttpStatus.OK);
    return [];
  }
}
