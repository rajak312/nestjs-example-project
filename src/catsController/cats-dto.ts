export class CreateCatDto {
  name: string;
  age: number;
  address?: string;
}
export class CreateDogDto {
  name: string;
  age: number;
  address?: string;
}
