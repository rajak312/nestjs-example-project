import { Controller, Get, Body, Post, Query } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { Dog } from 'src/dto/dog.dto';
import { Users } from 'src/enum/user.enum';
import { DogService } from 'src/services/dog.service';

@Controller('dog')
export class DogController {
  constructor(private readonly dogService: DogService) {}

  @Get('dogs')
  getAllCats() {
    return this.dogService.getAllDogs();
  }

  @Post('create')
  registerCat(@Body() dog: Dog) {
    return this.dogService.create(dog);
  }

  @Get('role')
  @ApiQuery({ name: 'role', enum: Users, isArray: true })
  async filterByRole(@Query('role') role: Users = Users.ADMIN) {
    return role;
  }
}
