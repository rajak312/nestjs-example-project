import { Controller, Get } from '@nestjs/common';
import { LionService } from 'src/services/lion.service';
import { TigerService } from 'src/services/tiger.service';

@Controller('lion')
export class LionController {
  constructor(
    private readonly lionService: LionService,
    private readonly tigerService: TigerService,
  ) {}

  @Get('lions')
  getAllLions() {
    return this.lionService.getLions();
  }

  @Get('tigers')
  getAllTigers() {
    return this.tigerService.getTigers();
  }
}
