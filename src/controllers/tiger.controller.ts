import { Controller, Get } from '@nestjs/common';
import { TigerService } from 'src/services/tiger.service';

@Controller('tiger')
export class TigerController {
  constructor(private readonly tigerService: TigerService) {}

  @Get('tigers')
  getAllTigers() {
    return this.tigerService.getTigers();
  }
}
