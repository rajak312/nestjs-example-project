define(['N/ui/serverWidget', 'N/search', 'N/record', 'N/file'], function (
  ui,
  search,
  record,
  file,
) {
  function getSearchResults(searchObj) {
    var searchResults = [];
    var searchPageSize = 1000;
    var searchPageCount = Math.ceil(
      searchObj.runPaged().count / searchPageSize,
    );

    for (var pageIndex = 0; pageIndex < searchPageCount; pageIndex++) {
      var searchPage = searchObj.runPaged({
        pageSize: searchPageSize,
        page: pageIndex,
      });
      searchPage.pageRanges.forEach(function (pageRange) {
        var currentPage = searchPage.fetch({
          index: pageRange.index,
        }).data;
        currentPage.forEach(function (result) {
          searchResults.push(result);
        });
      });
    }

    return searchResults;
  }

  function attachFilesToInvoices(selectedInvoices, selectedFiles) {
    return Promise.all(
      selectedInvoices.map(function (invoiceId) {
        return record.load
          .promise({
            type: record.Type.TRANSACTION,
            id: invoiceId,
          })
          .then(function (invoiceRecord) {
            return Promise.all(
              selectedFiles.map(function (fileId) {
                return record.load
                  .promise({
                    type: record.Type.FILE,
                    id: fileId,
                  })
                  .then(function (fileRecord) {
                    return invoiceRecord.attach.promise({
                      record: fileRecord,
                      fieldId: 'custbody_invoice_attachment',
                    });
                  });
              }),
            ).then(function () {
              return invoiceRecord.save.promise();
            });
          });
      }),
    );
  }

  function onRequest(context) {
    if (context.request.method === 'GET') {
      var form = ui.createForm({
        title: 'Attach Files to Vendor Invoices',
      });

      // Create a multiselect field to select vendor invoices
      var invoiceField = form.addField({
        id: 'custpage_invoices',
        type: ui.FieldType.MULTISELECT,
        label: 'Vendor Invoices',
      });

      // Create a search to get a list of vendor invoices
      var invoiceSearch = search.create.promise({
        type: search.Type.TRANSACTION,
        filters: [['mainline', 'is', 'T']],
        columns: ['internalid', 'tranid'],
      });

      // Get search results using getSearchResults function
      return invoiceSearch.then(function (invoiceSearchObj) {
        var invoiceResults = getSearchResults(invoiceSearchObj);

        // Add options to the multiselect field
        invoiceResults.forEach(function (result) {
          invoiceField.addSelectOption({
            value: result.getValue('internalid'),
            text: result.getValue('tranid'),
          });
        });

        // Create a multiselect field to select files
        var fileField = form.addField({
          id: 'custpage_files',
          type: ui.FieldType.MULTISELECT,
          label: 'Files',
        });

        // Create a search to get a list of files
        var fileSearch = search.create.promise({
          type: search.Type.FOLDER,
          filters: [['name', 'contains', 'Invoice Attachments']],
          columns: ['name', 'internalid'],
        });
      });

      // Get search results using getSearchResults function
      return fileSearch.then(function (fileSearchObj) {
        var fileResults = getSearchResults(fileSearchObj);

        // Add options to the multiselect field
        fileResults.forEach(function (result) {
          fileField.addSelectOption({
            value: result.getValue('internalid'),
            text: result.getValue('name'),
          });
        });

        // Add a submit button to the form
        form.addSubmitButton({
          label: 'Attach Files',
        });

        // Render the form
        context.response.writePage(form);
      });
    } else if (context.request.method === 'POST') {
      // Get the selected vendor invoices and files
      var selectedInvoices = context.request.parameters.custpage_invoices || [];
      var selectedFiles = context.request.parameters.custpage_files || [];

      // Attach the files to the vendor invoices
      return attachFilesToInvoices(selectedInvoices, selectedFiles)
        .then(function () {
          // Redirect back to the form with a success message
          var params = {
            successMsg: 'Files attached successfully!',
          };
          var suiteletUrl = record.URL.get({
            recordType: record.Type.SUITELET,
            scriptId: context.request.parameters.custscript_scriptid,
            deploymentId: context.request.parameters.custscript_deploymentid,
          });
          var redirectUrl =
            suiteletUrl +
            '&' +
            Object.keys(params)
              .map(function (key) {
                return (
                  encodeURIComponent(key) +
                  '=' +
                  encodeURIComponent(params[key])
                );
              })
              .join('&');
          context.response.sendRedirect({
            type: ui.MessageType.CONFIRMATION,
            title: 'Success',
            message: 'Files attached successfully!',
            redirectUrl: redirectUrl,
          });
        })
        .catch(function (error) {
          // Redirect back to the form with an error message
          var params = {
            errorMsg:
              'An error occurred while attaching files: ' + error.message,
          };
          var suiteletUrl = record.URL.get({
            recordType: record.Type.SUITELET,
            scriptId: context.request.parameters.custscript_scriptid,
            deploymentId: context.request.parameters.custscript_deploymentid,
          });
          var redirectUrl =
            suiteletUrl +
            '&' +
            Object.keys(params)
              .map(function (key) {
                return (
                  encodeURIComponent(key) +
                  '=' +
                  encodeURIComponent(params[key])
                );
              })
              .join('&');
          context.response.sendRedirect({
            type: ui.MessageType.ERROR,
            title: 'Error',
            message:
              'An error occurred while attaching files: ' + error.message,
            redirectUrl: redirectUrl,
          });
        });
    }
  }

  return {
    onRequest: onRequest,
  };
});
