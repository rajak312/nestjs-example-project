export enum Users {
  ADMIN = 'admin',
  MODERATOR = 'moderator',
  USER = 'user',
}
